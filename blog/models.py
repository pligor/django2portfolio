from django.db import models


# Create your models here.
class Blog(models.Model):
    image = models.ImageField(upload_to='images/')  # empty for now
    body = models.TextField()
    title = models.CharField(max_length=40)
    pub_date = models.DateTimeField()

    def summary(self):
        return self.body[:100]

    def pub_date_pretty(self):
        return self.pub_date.strftime('%b %e %Y')

    # Create blog models
    # title
    # pub_date
    # body
    # image

    # add blog app to settings
    # create migration
    # migrate
    # add to admin

    def __str__(self):
        return self.title
